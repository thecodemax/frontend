import { Component, OnInit } from '@angular/core';
import { AuthService } from './../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  type
  constructor(private routerService: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.type = this.authService.getType()
    if (this.type != 'admin')
      this.routerService.navigateByUrl('/')
  }
  logout() {
    this.authService.deleteToken()
    this.routerService.navigateByUrl('/')
  }

}
