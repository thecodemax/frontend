import { Component, OnInit } from '@angular/core';

import { ContactService } from './../../services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'email', 'message'];
  dataSource
  constructor(private contactService:ContactService) { }

  ngOnInit(): void {
    this.contactService.getAll().subscribe(res => this.dataSource = res)
  }

}
