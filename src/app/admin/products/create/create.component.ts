import { ProductsService } from './../../../services/products.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  public form: FormGroup;

  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      description: new FormControl('', [Validators.required, Validators.maxLength(1024)]),
      quantity: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      amount: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      image: new FormControl('', [Validators.required]),
      imageSource: new FormControl('', [Validators.required])
    });
  }
  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => this.form.patchValue({
        imageSource: reader.result
      })
    }
  }
  newProduct(): void {
    this.productsService.create(this.form.value).subscribe(
      res => console.log(res),
      error => console.error(error)
    );
  }
}
