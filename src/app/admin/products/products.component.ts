import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ProductsService } from './../../services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: any;
  dataSource
  baseurl = environment.baseUrl+'/images/'
  constructor(private productService: ProductsService) { }
  displayedColumns: string[] = ['id', 'name', 'quantity', 'amount'];

  ngOnInit(): void {
    this.productService.getAll().subscribe(res => {
      this.products = res;
      this.products.map((item) => item.image = JSON.parse(item.image));
      this.dataSource = this.products
    })
  }

}
