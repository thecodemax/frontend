import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  options
  constructor(private http: HttpClient, private authService:AuthService) { this.options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: 'Bearer ' + this.authService.getToken()
      })
    };}
  getAll() {
    const path = environment.baseUrl + '/products'
    return this.http.get(path);
  }
  get(product: string) {
    const path = environment.baseUrl + '/products/'+ product
    return this.http.get(path);
  }
  create(data) {
    const path = environment.baseUrl + '/products'
    return this.http.post(path, data, this.options)
  }
}
