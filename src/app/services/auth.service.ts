import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }
  login(params) {
    const path = environment.baseUrl + '/auth/login'
    return this.http.post(path, params)
  }
  register(params) {
    const path = environment.baseUrl + '/auth/register'
    return this.http.post(path, params)
  }
  getToken() {
    return localStorage.getItem('token')
  }
  getType() {
    return localStorage.getItem('type')
  }
  setToken(data) {
    localStorage.setItem('token', data.access_token)
    localStorage.setItem('expires_in', Date.now() + data.expires_in)
    localStorage.setItem('type', data.type)
    return data.type
  }
  deleteToken() {
    localStorage.removeItem('token')
    localStorage.removeItem('expires_in')
    localStorage.removeItem('type')
  }
}
