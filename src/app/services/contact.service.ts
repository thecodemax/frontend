import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  options
  constructor(private http: HttpClient, private authService:AuthService) {
    this.options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: 'Bearer ' + this.authService.getToken()
      })
    };
  }
  create(params) {
    const path = environment.baseUrl + '/contacts'
    return this.http.post(path, params)
  }
  getAll() {
    const path = environment.baseUrl + '/contacts'
    return this.http.get(path, this.options)
  }
}
