import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private storageSub = new Subject<string>();
  constructor(private http: HttpClient) { }
  watchStorage(): Observable<any> {
    return this.storageSub.asObservable();
  }
  getItem(key) {
    return sessionStorage.getItem(key);
  }
  setItem(key: string, data: any) {
    sessionStorage.setItem(key, data);
    this.storageSub.next("changed");
  }
  removeItem(key) {
    sessionStorage.removeItem(key);
    this.storageSub.next("changed");
  }
  newOrder(data) {
    const path = environment.baseUrl + "/order";
    const formData = new FormData();
    formData.append("name", data.name);
    formData.append("phone", data.phone);
    formData.append("email", data.email);
    formData.append("address", data.address);
    formData.append("message", data.message);
    formData.append("items", JSON.stringify(data.cart));
    return this.http.post(path, formData);
  }

}
