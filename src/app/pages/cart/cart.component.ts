import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { CartService } from "src/app/services/cart.service";
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cart: any;
  cartCount: number;
  cartAmount: number;
  baseurl = environment.baseUrl + '/images/'
  constructor(
    private routerService: Router,
    private cartService: CartService,
    private authService: AuthService
  ) {
    if (this.cartService.getItem("cart")) {
      this.cart = JSON.parse(this.cartService.getItem("cart"));
      this.cartCount = this.cart.length;
    }
  }


  ngOnInit(): void {
    this.cartService.watchStorage().subscribe((data: string) => {
      if (this.cartService.getItem("cart")) {
        this.cart = JSON.parse(this.cartService.getItem("cart"));
        this.cartCount = this.cart.length;
      }
    });
    if(!this.authService.getType())
      this.routerService.navigateByUrl('/')
  }
addQuantity(item): void {
    item.quantity = parseInt(item.quantity, 10) + 1;
    this.cartService.setItem("cart", JSON.stringify(this.cart));
  }
  removeQuantity(item): void {
    if (parseInt(item.quantity, 10) > 1) {
      item.quantity = parseInt(item.quantity, 10) - 1;
      this.cartService.setItem("cart", JSON.stringify(this.cart));
    }
  }
  removeProduct(item) {
    this.cart = this.cart.filter(
      (data) => data.product.id !== item.product.id
    );
    this.cartService.setItem("cart", JSON.stringify(this.cart));
  }
  order() {
    alert('Implement order processing ---- pending')
  }
}
