import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { ContactService } from './../../services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public form: FormGroup;

  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      email: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      message: new FormControl('', [Validators.required, Validators.maxLength(1024)]),
    });
  }
  submit() {
    this.contactService.create(this.form.value).subscribe(
      res => console.log(res),
      error => console.error(error)
    );
  }
}
