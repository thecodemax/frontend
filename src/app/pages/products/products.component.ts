import { environment } from './../../../environments/environment.prod';
import { Component, OnInit } from '@angular/core';
import { ProductsService } from './../../services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: any;
  baseurl = environment.baseUrl+'/images/'
  constructor(
    private productService: ProductsService
  ) { }

  ngOnInit(): void {
    this.productService.getAll().subscribe(res => {
      this.products = res;
      this.products.map((item) => item.image = JSON.parse(item.image));
    })
  }
}
