import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, Validators, FormGroup } from "@angular/forms";

import { CartService } from '../../../services/cart.service';
import { ProductsService } from './../../../services/products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  product?: any
  baseurl = environment.baseUrl + '/images/'
  constructor(
    private route: ActivatedRoute,
    private productService: ProductsService,
    private cartService: CartService
  ) { }
  form: FormGroup;
  ngOnInit(): void {
    let slug = ''
    this.route.paramMap.subscribe((params) => {
      slug = params.get('product')
    });
    this.form = new FormGroup({
      quantity: new FormControl("1", [Validators.required]),
    });
    this.productService.get(slug).subscribe(res => {
      this.product = res
      this.product.image = JSON.parse(this.product.image)
    })
  }
addToCart() {
    let items = [];
    let flag = true;
    if (this.cartService.getItem("cart")) {
      items = JSON.parse(this.cartService.getItem("cart"));
    }
    const item = {
      product: this.product,
      quantity: this.form.controls.quantity.value,
    };
    items.filter((row) => {
      if (row.product.id === item.product.id) {
        row.quantity = parseInt(row.quantity, 10) + parseInt(item.quantity, 10);
        flag = false;
      }
    });
    if (flag) {
      items.push(item);
    }
    this.cartService.setItem("cart", JSON.stringify(items));
  }
  addQuantity(): void {
    this.form.controls.quantity.setValue(
      parseInt(this.form.controls.quantity.value, 10) + 1
    );
  }
  removeQuantity(this): void {
    if (parseInt(this.form.controls.quantity.value, 10) > 1) {
      this.form.controls.quantity.setValue(
        parseInt(this.form.controls.quantity.value, 10) - 1
      );
    }
  }

}
