import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './../services/auth.service';
import { CartService } from "src/app/services/cart.service";

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  cartCount: number
  cartAmount: number
  type: string
  siteLanguage: string = 'English';
siteLocale: string;
languageList = [
{ code: 'en', label: 'English' },
{ code: 'fr', label: 'Français' },
];

  constructor(
    private routerService: Router,
    private cartService: CartService,
    private authService: AuthService
  ) {}
  ngOnInit() {
    this.cartService.watchStorage().subscribe((data: string) => {
      if (this.cartService.getItem("cart")) this.populate()
    });
    if (this.cartCount == undefined && JSON.parse(this.cartService.getItem("cart")))
      this.populate()
    this.type = this.authService.getType()
    this.siteLocale = window.location.pathname.split('/')[1];
    this.siteLanguage = this.languageList.find(f => f.code === this.siteLocale).label;
  }
  populate() {
    const cart =  JSON.parse(this.cartService.getItem("cart"))
    this.cartCount = cart.length
    this.cartAmount = 0
      cart.map(item => this.cartAmount += item.quantity * item.product.amount)
  }
  search() {
        alert('Implement Search products ---- pending')
  }
  logout() {
    this.authService.deleteToken()
    this.routerService.navigateByUrl('/')
  }
}
