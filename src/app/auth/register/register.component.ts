import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public loginForm: FormGroup
  constructor(private routerService: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      email: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      password_confirmation: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      g_recaptcha_response: new FormControl('', [Validators.required])
    });
  }
  register(captcha) {
    let data = this.loginForm.value
    data['g-recaptcha-response'] = captcha
    console.log(data);
    this.authService.register(data).subscribe(res => {
      this.authService.setToken(res)
      this.routerService.navigateByUrl('/')
    }, error => console.error(error))
  }
  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.register(captchaResponse)
  }
}
