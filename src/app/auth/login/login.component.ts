import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup
  constructor(private routerService: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(100)])
    });
  }
  login() {
    this.authService.login(this.loginForm.value).subscribe(res => {
      const type =this.authService.setToken(res)
      this.routerService.navigateByUrl(type == 'admin'?'/admin':'/')
    }, error => console.error(error))
  }
}
